import createLambdaAPI from 'lambda-api';

const createAPI = ({
  routes = [],
  base = `/${process.env.BASE_PATH}/${process.env.VERSION}`,
}) => {
  const api = createLambdaAPI({ base });
  console.log('create API');
  api.use('*', (req, res, next) => {
    res.cors();
    next();
  });

  routes.forEach((route) => {
    // eslint-disable-next-line new-cap
    console.log('route', route);
    api.METHOD(...route);
  });
  return api;
};

export default createAPI;
