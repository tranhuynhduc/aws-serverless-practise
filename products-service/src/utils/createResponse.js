/**
 * @typedef Response
 * @property {Boolean} success.required - Result of operation
 * @property {string} message.required - Message of operation
 * @property {Number} errorCode - Error code
 * @property {*} data.required - Results data
 */
/**
 * Create standard response for Rest API
 *
 * @export
 * @param {Boolean} success.required - Operation result
 * @param {string} message.required - Operation message
 * @param {*} data - Response data
 * @param {Number} errorCode - Error code
 * @return {Response}
 */
export default function createResponse(success, message, data, errorCode) {
  return {
    success,
    message,
    errorCode,
    data,
  };
}

/**
 * Create standard response for GET method
 *
 * @export
 * @param {string} message.required - Operation message
 * @param {*} data - Response data
 * @return {Response}
 */
export const createGETResponse = (message, data) => {
  return createResponse(true, message, data);
};

/**
 * Create standard response for POST method
 *
 * @export
 * @param {string} message.required - Operation message
 * @param {*} data - Response data
 * @return {Response}
 */
export const createPOSTResponse = (message, data) => {
  return createResponse(true, message, data);
};

/**
 * Create standard response for DELETE method
 *
 * @export
 * @param {string} message.required - Operation message
 * @return {Response}
 */
export const createDELETEResponse = (message) => {
  return createResponse(true, message, {});
};

/**
 * Create standard response for POST method
 *
 * @export
 * @param {string} message.required - Operation message
 * @param {Number} errorCode.required - Error code
 * @return {Response}
 */
export const createErrorResponse = (message, errorCode) => {
  return createResponse(false, message, {}, errorCode);
};
