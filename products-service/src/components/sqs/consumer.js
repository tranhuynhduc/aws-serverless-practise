var AWS = require('aws-sdk');
AWS.config.update({region: 'ap-southeast-1'});

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const tableName = `${process.env.DYNAMODB_PRODUCTS_TABLE}-${process.env.STAGE || "dev"}`;
const attributesToGet = [ 'id', 'productName', 'ordered'];
const getParams = (opts) => {
    return {
      TableName: tableName,
      AttributesToGet: attributesToGet,
      ...opts
    };
  };
let counter = 1;
let messageCount = 0;
let funcId = 'id'+parseInt(Math.random()*1000);

export const handler = async (event) => {
    // Record number of messages received
    if (event.Records) {
        messageCount += event.Records.length;
    }
    console.log(funcId + ' REUSE: ', counter++);
    console.log('Message Count: ', messageCount);
    console.log(JSON.stringify(event));

    const timestamp = new Date().getTime();
    const productID = event.Records[0].messageAttributes.productID.stringValue;
    console.log('productID', productID);
    const params = getParams({
      Key: {
        id: productID
      },
      UpdateExpression: 'set ordered = ordered + :value, updatedAt = :up',
      ExpressionAttributeValues: {
        ':value': 1,
        ':up': timestamp,
      },
      ReturnValues: 'ALL_NEW'
    });

    return dynamoDb
      .update(params)
      .promise()
      .then((data) => {
        console.log("update product success");
      })
      .catch(({ message }) =>
        console.log('false to update product', message, params)
      );
};
