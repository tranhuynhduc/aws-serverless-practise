import {
  createPOSTResponse,
  createGETResponse,
  createErrorResponse,
} from "../../utils/createResponse";
import AWS from "aws-sdk";
import { v1 as uuidv1 } from "uuid";

const dynamoDb = new AWS.DynamoDB.DocumentClient();

const tableName = `${process.env.DYNAMODB_PRODUCTS_TABLE}-${process.env.STAGE || "dev"}`;
const attributesToGet = [ 'id', 'productName', 'ordered'];
const getParams = (opts) => {
  return {
    TableName: tableName,
    AttributesToGet: attributesToGet,
    ...opts
  };
};
export const getProduct = (req, res) => {
  const { id } = req.params;
  const params = getParams({
    Key: { id }
  });

  return dynamoDb
    .get(params)
    .promise()
    .then((data) =>
      res.json(
        createGETResponse("Get product success", data)
      )
    );
};
export const getProducts = (req, res) => {
  const params = getParams();

  return dynamoDb
    .scan(params)
    .promise()
    .then((data) =>
      res.json(
        createGETResponse("Get products success", data)
      )
    );
};

export const createProduct = (req, res) => {
  const timestamp = new Date().getTime();
  const { productName } = req.body;

  if (typeof productName !== "string" || productName === "") {
    return res
      .status(400)
      .send(createErrorResponse("Product must be a string", 400));
  }

  const params = getParams({
    Item: {
      id: uuidv1(),
      productName,
      stock: 10,
      ordered: 0,
      createdAt: timestamp,
      updatedAt: timestamp,
    },
  });

  return dynamoDb
    .put(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Product created", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};

export const updateProduct = (req, res) => {
  const timestamp = new Date().getTime();
  const { id } = req.params;
  const { productName } = req.body;

  if (typeof productName !== "string" || productName === "") {
    return res
      .status(400)
      .send(createErrorResponse("Product must be a string", 400));
  }

  const params = getParams({
    Key: {
      id
    },
    UpdateExpression: 'set productName = :value, updatedAt = :up',
    ExpressionAttributeValues: {
      ':value': productName,
      ':up': timestamp,
    },
    ReturnValues: 'ALL_NEW'
  });

  return dynamoDb
    .update(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Product updated", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};

export const deleteProduct = (req, res) => {
  const { id } = req.params;

  const params = getParams({
    Key: {
      id
    },
  });

  return dynamoDb
    .delete(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Product deleted", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};
