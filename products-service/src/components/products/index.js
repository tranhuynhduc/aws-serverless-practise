import createHandler from "../../utils/createHandler";
import routes from "./products.routes";

export const handler = createHandler(routes);