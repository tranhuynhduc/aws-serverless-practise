import {
  createProduct, getProduct, getProducts, updateProduct, deleteProduct,
} from './product.controller';
import createRouter from '../../utils/createRouter';

const routes = createRouter('/products')
  .get(getProducts)
  .post(createProduct)
  .changeRoute('/products/:id')
  .get(getProduct)
  .put(updateProduct)
  .delete(deleteProduct)
  .getRoutes();

export default routes;
