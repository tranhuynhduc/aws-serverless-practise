// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "ap-southeast-1" });

// Create an SQS service object
var sqs = new AWS.SQS({ apiVersion: "2012-11-05" });


export const sendCreateOrderMessage = async (productID = "123") => {
  console.log("get QueueUrl");

  const messageParams = {
    // Remove DelaySeconds parameter and value for FIFO queues
    DelaySeconds: 10,
    MessageAttributes: {
      productID: {
        DataType: "String",
        StringValue: productID,
      },
    },
    MessageBody: "Create orders",
    // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
    // MessageGroupId: "Group1",  // Required for FIFO queues
    QueueUrl:
      "https://sqs.ap-southeast-1.amazonaws.com/272448497697/product-queue",
  };
  console.log(
    "queue url",
    "https://sqs.ap-southeast-1.amazonaws.com/272448497697/product-queue"
  );
  return sqs.sendMessage(messageParams, function (err, data) {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("Success send message", data);
    }
  });
};
