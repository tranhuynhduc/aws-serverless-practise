import { createGETResponse } from './createResponse';

export const getNameByLang = (language, names, defaultName = '') =>
  names?.find(({ lang }) => lang === language)?.name || defaultName;

export const localize = (items = [], acceptLanguage = '') =>
  items.map((item) => {
    const itemObject = item.toObject();
    const { names, name } = itemObject;
    return {
      ...itemObject,
      name: getNameByLang(acceptLanguage, names, name),
    };
  });

export const responseLocalize = (res, acceptLanguage, entityName) => (
  items
) => {
  if (!items || items?.length === 0) {
    return res.json(createGETResponse(`${entityName} not found.`, items));
  }
  if (!acceptLanguage) {
    return res.json(createGETResponse(`${entityName} found.`, items));
  }
  return res.json(
    createGETResponse(`${entityName} found.`, localize(items, acceptLanguage))
  );
};
