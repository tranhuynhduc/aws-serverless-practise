import createAPI from './createAPI';

const createHandler = (routes) => async (
  event,
  context
) => {
  try {
    const api = createAPI({ routes });
    console.log('api run');
    return await api.run(event, context);
  } catch (e) {
    console.log('error', e);
    return process.exit(0);
  }
};

export default createHandler;
