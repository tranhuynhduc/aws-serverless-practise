const createRouter = (route, routes = []) => {
  const addMethod = (method) => (...midleware) =>
    createRouter(route, [...routes, [method, route, ...midleware]]);
  return {
    get: addMethod('get'),
    post: addMethod('post'),
    delete: addMethod('delete'),
    patch: addMethod('patch'),
    put: addMethod('put'),
    changeRoute: (newRoute) => createRouter(newRoute, routes),
    getRoutes: () => routes,
  };
};

export default createRouter;
