import createHandler from "../../utils/createHandler";
import routes from "./orders.routes";

export const handler = createHandler(routes);