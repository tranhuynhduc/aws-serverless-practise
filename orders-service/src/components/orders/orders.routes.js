import {
  createOrder, getOrder, getOrders, updateOrder, deleteOrder,
} from './orders.controller';
import createRouter from '../../utils/createRouter';

const routes = createRouter('/orders')
  .get(getOrders)
  .post(createOrder)
  .changeRoute('/orders/:id')
  .get(getOrder)
  .put(updateOrder)
  .delete(deleteOrder)
  .getRoutes();

export default routes;
