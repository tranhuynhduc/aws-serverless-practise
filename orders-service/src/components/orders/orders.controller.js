import {
  createPOSTResponse,
  createErrorResponse,
  createGETResponse,
} from "../../utils/createResponse";
import AWS from "aws-sdk";
import { v1 as uuidv1 } from "uuid";
import { sendCreateOrderMessage } from "../../sqs/sendMessage";

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const tableName = `${process.env.DYNAMODB_ORDERS_TABLE}-${process.env.STAGE || "dev"}`;
const attributesToGet = ["id", "productID"];
const getParams = (opts) => {
  return {
    TableName: tableName,
    AttributesToGet: attributesToGet,
    ...opts
  };
};
export const getOrder = (req, res) => {
  const { id } = req.params;
  const params = getParams({
    Key: { id },
  });

  return dynamoDb
    .get(params)
    .promise()
    .then((data) => res.json(createGETResponse("Get order success", data)));
};

export const getOrders = (req, res) => {
  const params = getParams();
  return dynamoDb
    .scan(params)
    .promise()
    .then((data) => res.json(createGETResponse("Get orders success", data)))
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};

export const createOrder = (req, res) => {
  const timestamp = new Date().getTime();
  const { productID } = req.body;

  if (!productID) {
    return res
      .status(400)
      .send(createErrorResponse("please provide product ID", 400));
  }

  const params = getParams({
    Item: {
      id: uuidv1(),
      productID,
      createdAt: timestamp,
      updatedAt: timestamp,
    },
  });
  try {
    sendCreateOrderMessage(productID);

  } catch ({message}) {
    res.status(400).send(createErrorResponse({
      custom: "can't send message",
      message
    }, 400));
  }

  return dynamoDb
    .put(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Order created", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};

export const updateOrder = (req, res) => {
  const timestamp = new Date().getTime();
  const { id } = req.params;
  const { productName } = req.body;

  if (typeof productName !== "string" || productName === "") {
    return res
      .status(400)
      .send(createErrorResponse("Order must be a string", 400));
  }

  const params = getParams({
    Key: {
      id,
    },
    UpdateExpression: "set productName = :value, updatedAt = :up",
    ExpressionAttributeValues: {
      ":value": productName,
      ":up": timestamp,
    },
    ReturnValues: "ALL_NEW",
  });

  return dynamoDb
    .update(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Order updated", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};

export const deleteOrder = (req, res) => {
  const { id } = req.params;

  const params = getParams({
    Key: {
      id,
    },
  });

  return dynamoDb
    .delete(params)
    .promise()
    .then((data) => {
      return res.json(createPOSTResponse("Order deleted", data));
    })
    .catch(({ message }) =>
      res.status(400).send(createErrorResponse(message, 400))
    );
};
